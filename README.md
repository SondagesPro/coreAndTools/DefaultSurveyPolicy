# DefaultSurveyPolicy

Replace the default survey email templates text globally.

This version is compatible was tested in LimeSurvey version 3.27.2 and must be compatible with other version 3.X.

This version was not tested on LimeSurvey 4.X or 5.X version, but can be compatible.

## Usage

You can choose the default way to show survey policy

You can update default label, message and error message.

If you fill slabel, message or error message : it was used to replace all default policy text, in all language and settings.

## Copyright
- Copyright ©  2021 Denis Chenu <http://www.sondages.pro>
- Copyright ©  2021 SGAR21 / Secrétariat général pour les affaires régionales - Bourgogne-Franche-Comté
- Licence : AGPL v3 <https://www.gnu.org/licenses/agpl-3.0.html>
