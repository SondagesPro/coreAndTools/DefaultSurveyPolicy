<?php

/**
 * Set the default policy
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021 Denis Chenu <http://www.sondages.pro>
 * @copyright 2021 SGAR21 / Secrétariat général pour les affaires régionales - Bourgogne-Franche-Comté
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class DefaultSurveyPolicy extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Set the Default Survey policy state and text.';
    protected static $name = 'DefaultSurveyPolicy';
    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => '<div class="">' .
                 '<p class="well">If you leave empty string : default limesurvey text still used.</p>' . 
                 '<p class="alert alert-danger"><strong>Warning:</strong> Same text is used for all languages.</p>' .
                 '<p class="alert alert-info">No HTML editor, you can copy/paste HTML source if needed. You have to add yourself html tag.</p>' .
                 '</div>',
        ),
        'showsurveypolicynotice' => array(
            'type' => 'select',
            'label' => 'Show survey policy text with mandatory checkbox:',
            'default' => 0,
            'options' => array(
                '0' => "Don't show",
                '1' => "Inline text",
                '2' => "Collapsible text",
            )
        ),
        'surveyls_policy_notice_label' => array(
            'type' => 'string',
            'label' => 'Survey data policy checkbox label:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
                'maxlength' => 192
            ),
        ),
        'surveyls_policy_notice' => array(
            'type' => 'text',
            'label' => 'Survey data policy message:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
        'surveyls_policy_error' => array(
            'type' => 'text',
            'label' => 'Survey data policy error message:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
    );
    /** @inheritdoc **/
    public function init()
    {
        $this->subscribe('beforeSurveyLanguageSettingSave', 'setDefaultPolicyText');
        $this->subscribe('beforeSurveySave', 'setDefaultPolicy');
    }

    public function setDefaultPolicyText()
    {
        $model = $this->getEvent()->get('model');
        if($model->isNewRecord){
            $settings = array(
                'surveyls_policy_notice_label',
                'surveyls_policy_notice',
                'surveyls_policy_error',
            );
            foreach ($settings as $setting) {
                if (!empty($this->get($setting))) {
                    $model->setAttribute($setting, $this->get($setting) );
                }
            }
        }
    }
    public function setDefaultPolicy()
    {
        $model = $this->getEvent()->get('model');
        if($model->isNewRecord){
            if (!is_null($this->get('showsurveypolicynotice'))) {
                $model->showsurveypolicynotice = $this->get('showsurveypolicynotice');
            }
        }
    }
    
}
